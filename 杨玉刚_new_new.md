## 杨玉刚
 * 手机：18612184602                   
 * 邮箱：<smartydroid@gmail.com>
 
## 个人特点
 * 热爱技术：每天50%的时间花在技术上
 * 代码洁癖：隔段时间就会 review 代码，整理代码格式，重构代码设计
 * 学习能力非常强：如iOS、PHP等其他新技术可在很短时间（短则一天，多则半月）内了解语法后，搭建出最新最好的框架开发
 * 团队管理能力较强：负责过15个人的技术团队

## 专业技能
 * 五年 Android 开发经验，两年 iOS 开发经验，两年 PHP 开发经验。
 * Android：精通UI、网络层、Framework框架、熟悉常用的开源框架。可自定义开源框架，能独立快速设计开发Android客户端。
 * iOS：可设计技术框架，能独立快速开发iOS客户端。
 * PHP：精通 Laravel 框架，熟悉 CI 框架，可独立设计开发 API、后台。
 * 熟悉设计模式，具有扎实的Java基础和良好的编程习惯。
 * 熟悉Linux基本命令使用，能够独立部署服务端环境。
 * 熟悉MySql、SQLite3数据库的使用，能够独立设计数据库。

### 2014.03 - 2015.09：北京绿葡科技有限公司
* 职位：技术总监，持有公司5%的股份
* 工作内容：
	- 负责公司整个技术团队招聘，提升团队技术研发能力
	- 负责公司Android产品框架设计，设计一套可快速开发产品的Android应用框架，源码托管在[github][1]
		1. 封装网络请求框架，对服务器callback进行统一处理，减少冗余代码
		2. 封装ListFragment，实现 List 只需继承该 Fragment，设置布局 xml、viewholder、数据源即可展现 List。
		3. 封装了工具类，可快速获取常用信息，如AppInfo（deviceId、version、versionCode、channel、screenWidth、screenHeight、OS）、WeakAsyncTask等 
  	- 负责公司服务器开发和框架整理
     	1. 基于Laravel 4.2搭建后台框架，源码托管在[bitbucket][2]
	   2. 基于Laravel 5.1搭建后台框架，源码托管在[github][19]
  	- 负责iOS基础框架及客户端研发
  	   1. 设计 iOS 基础框架，同 Android 基础框架，使用该框架可快速开发 iOS客户端
	- 可点击项目名称访问对应项目客户端 
  		1. 2015.03 - 2015.04：[创业箱][8]
     	2. 2015.01 - 2015.03：[聚店][10]
    	3. 2014.12 - 2015.02：[六六货运][9]
    	4. 2014.08 - 2014.12：[云借阅][16]
    	5. 2014.11 - 2014.12：[嘀嗒][7]
    	6. 2014.08 - 2014.10：[段子来了][6]
    	7. 2014.05 - 2014.08：[多彩日记][15]
    	8. 2014.04 - 2014.06：[明星互动][14]
    	9. 2014.04 - 2014.05：[爱互动][13]
    	10. 2014.03 - 2014.04：[测评工具][12]
    	11. 2014.03 - 2014.06：[人社资源][11]
* 离职原因：职业发展方向与公司运营模式不一致

### 2013.05 - 2014.03：北京友宝昂莱科技有限公司
* 职位：Android研发部门经理
* 工作内容：
	- [友乐下载Android客户端工厂版V1.0][17]  
		1. 重构List相关代码，分离出公用的ListFramgent，重用资源列表
      	2. 采用Github的网络请求库，该请求框架采用反射机制返回已解析的对象
      	3. 采用SQLite，对数据缓存进行统一处理
      	4. 采用Android-Universal-Image-Loader异步加载图片
      	5. 采用DownloadProvider处理断点下载功能，定制下载线程部分的代码（Android 2.3不支持自定义下载线程的个数）、wifi切换到3g自动暂停等
  	- [友乐下载Android客户端公交版V1.1][18]
    	1. 网络请求框架更换为Volley，缓存数据废弃SQLite，而直接采用Volley的缓存机制
  	- [友乐下载Android客户端V2.0][17]
     	1. 解决低内存手机容易产生内存溢出的问题，采用MAT分析内存使用情况
     	2. 优化布局，如减少ListView的item层次结构、尽量使用相对布局、公用部分尽量采用include标签等
     	3. 优化代码，如DownloadList里面使用LoaderManager异步加载数据，使用StringBuilder拼接字符串等
	- 友乐下载后台管理系统：采用 PHP 开发，使用的OpenCart框架，参与部分后台功能开发
     	1. 设计开发权限管理
     	2. 资源的增删改查，包括小说、电影、电视剧、MP3
     	3. 数据统计功能，如某台售货机上小说的下载量、下载人数等
* 离职原因：公司内部项目失败，人员重组

### 2011.12 - 2013.05：宜邻美食
* 职位：创业合伙人
* 工作内容：
	- 宜邻美食iPhone用户端
		1. 功能主要是送货区域浏览、店铺列表、店铺菜品列表、用户订单相关功能
		2. 学习Objective-C，能够利用xib和storybord开发一些简单的iOS app
  	- 宜邻美食Android用户端
		1. 功能主要是送货区域浏览、店铺列表、店铺菜品列表、用户订单相关功能
		2. 该项目第一次尝试独立搭建项目框架、封装网络请求
  	- 宜邻美食Android商家端
		1. 功能主要是菜品添加删除修改列表、店铺管理、订单管理
		2. 项目难点在于订单推送，如何及时、不漏单将订单推送到商家客户端。项目采用MQTT来进行推送，通过MQTT的学习和使用，深刻理解一些关于HTTP请求的知识
		3. 该项目是第一次独立开发完成的Android项目，项目中初次使用Fragment，通过Fragment的使用，深刻理解Fragment相关知识。通过该项目锻炼，学会如何查找问题原因并解决问题
  	- 宜邻美食php后台及API
		1. 根据手机需要接口撰写API相关的代码
		2. 负责了后台一些简单的添加删除修改列表
* 离职原因：创业失败

### 2010.10 - 2011.12：成都凌点科技
* 职位：Android 开发工程师
* 工作内容：
	- YuePad项目
		1. YuePad是面向音乐家来设计的，设计的目的在于以后演出或者学习时，不需要带着乐谱，而只需要带有一个pad就可以进行音乐的演奏
		2. 主要负责开发存储/查找/删除MP3和乐谱文件以及MP3播放、歌词同步等功能
  	- Andrioid Launcher2.2源码进行定制开发
		1. 主要对桌面循环滑动、长按app进行位置互换
		2. 通过该项目，阅读很多Android源码，理解View的事件分发、，ContentProvider、自定义控件等知识
* 离职原因：离开成都，想去北上广发展

## 教育背景
 * 学历：本科 四川省攀枝花学院
 * 专业：计算机科学与技术
 * 毕业时间：2011-07-01
 
## 其他项目
* 2013.12 - 2014.03：[减肥帮Android客户端][3]
* 2014.01 - 2014.03：[减肥帮iOS客户端][4]
* 2013.10 - 2013.11：[掌上城市Android客户端][5] 
  
  [1]:https://github.com/loopeer/android-starter-kit
  [2]:https://bitbucket.org/loopeer/laputa4php
  [3]:http://www.wandoujia.com/apps/cn.jfbang
  [4]:https://itunes.apple.com/us/app/jian-fei-bang/id837111939?ls=1&mt=8
  [5]:http://fir.im/a72n/info
  [6]:http://fir.im/dzll
  [7]:http://fir.im/didaaa
  [8]:http://fir.im/startuptool
  [9]:http://fir.im/apdt
  [10]:http://fir.im/cds4
  [11]:http://fir.im/xcalss
  [12]:http://fir.im/8w42
  [13]:http://www.wandoujia.com/apps/com.onairm.android
  [14]:http://fir.im/7kzx
  [15]:http://fir.im/rzzb
  [16]:http://fir.im/66zz
  [17]:http://fir.im/oe44
  [18]:http://fir.im/asbu
  [19]:https://github.com/ForoneTech/ForoneAdmin

