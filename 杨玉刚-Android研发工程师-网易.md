## 杨玉刚

 * 手机：18612184602                   
 * 邮箱：<smartydroid@gmail.com>
 
## 个人特点

 * 热爱技术：每天50%的时间花在技术上
 * 代码洁癖：隔段时间就会 review 代码，整理代码格式，重构代码设计
 * 学习能力强：IOS、PHP等其他新技术都是在很短时间（短则一天，多则半月）通过浏览官方文档自学

## 专业技能

 * 五年 Android 开发经验，两年 iOS 开发经验，两年 PHP 开发经验
 * Android：熟悉UI、基础框架、常用的开源框架，可自定义开源框架，能独立设计开发客户端
 * IOS：可独立开发简单的客户端
 * PHP：熟悉 Laravel 框架，了解 CI 框架，可独立设计开发 API、后台功能
 * 熟悉设计模式，具有扎实的Java基础和良好的编程习惯
 * 熟悉Linux基本命令使用，能够独立部署服务端环境
 * 熟悉MySql、SQLite3数据库的使用，能够编写常用的SQL语句

## 工作经历
 
 * 工作经历的公司名称虽一年一变，并非我不稳定，四年来我一直与同一群人做事，主要是创业项目死的太快，需不断调整团队方向和资金来源
 * 现因家庭等诸多因素想找个较稳定的团队做事，如果团队还充满创业热情和激情就更好了
 * 最后感谢您花时间阅读我的简历，期待能有机会和您共事。

### 2014.03 - 2015.09

* 工作内容：
	- 负责公司Android产品框架设计，设计一套可快速开发产品的Android应用框架，源码托管在[github][1]
		1. 基于Retrofit封装网络请求框架，对服务器callback进行统一处理，这样网络请求和UI代码分离开来，Fragment只需负责UI展现，减少很多冗余代码
    	2. 封装常用UI组件：CollectionView（Google IO）、ProgressLoading、ResourceLoadingIndicator。 
  		3. BasePagedCollectionFragment利用DataLoader封装了上拉刷新和下拉加载更多，只需要继承BasePagedCollectionFragment实现父类方法即可完成列表功能 
     	4. 封装了工具类，可快速获取常用信息，如AppInfo（deviceId、version、versionCode、channel、screenWidth、screenHeight、OS）、WeakAsyncTask等 
     	5. 项目中其他开源库：Square的Picasso，JakeWharton的butterknife 
  	- 负责公司服务器开发和框架整理
     	1. 基于Laravel 4.2搭建后台框架，源码托管在[bitbucket][2]
	   	2. 基于Laravel 5.1搭建后台框架，源码托管在[github][19]
  	- 负责组建iOS技术团队并实施开发工作
  		1. 指导iOS实习生开发，解决常见的iOS问题
  		2. iOS开发采用第三方的AutoLayout框架（Mantle）
	- 可点击项目名称访问对应项目客户端 
  		1. 2015.03 - 2015.04：[创业箱][8]
     	2. 2015.01 - 2015.03：[聚店][10]
    	3. 2014.12 - 2015.02：[六六货运][9]
    	4. 2014.08 - 2014.12：[云借阅][16]
    	5. 2014.11 - 2014.12：[嘀嗒][7]
    	6. 2014.08 - 2014.10：[段子来了][6]
    	7. 2014.05 - 2014.08：[多彩日记][15]
    	8. 2014.04 - 2014.06：[明星互动][14]
    	9. 2014.04 - 2014.05：[爱互动][13]
    	10. 2014.03 - 2014.04：[测评工具][12]
    	11. 2014.03 - 2014.06：[人社资源][11]

### 2013.05 - 2014.03：北京友宝昂莱科技有限公司

* 工作内容：
	- [友乐下载Android客户端工厂版V1.0][17]  
		1. 重构List相关代码，分离出公用的ListFramgent，重用资源列表
      	2. 采用Github的网络请求库，该请求框架采用反射机制返回已解析的对象
      	3. 采用SQLite，对数据缓存进行统一处理
      	4. 采用Android-Universal-Image-Loader异步加载图片
      	5. 采用DownloadProvider处理断点下载功能，定制下载线程部分的代码（Android 2.3不支持自定义下载线程的个数）、wifi切换到3g自动暂停等
  	- [友乐下载Android客户端公交版V1.1][18]
    	1. 网络请求框架更换为Volley，缓存数据废弃SQLite，而直接采用Volley的缓存机制
  	- [友乐下载Android客户端V2.0][17]
     	1. 解决低内存手机容易产生内存溢出的问题，采用MAT分析内存使用情况
     	2. 优化布局，如减少ListView的item层次结构、尽量使用相对布局、公用部分尽量采用include标签等
     	3. 优化代码，如DownloadList里面使用LoaderManager异步加载数据，使用StringBuilder拼接字符串等
	- 友乐下载后台管理系统：采用 PHP 开发，使用的OpenCart框架，参与部分后台功能开发
     	1. 设计开发权限管理
     	2. 资源的增删改查，包括小说、电影、电视剧、MP3
     	3. 数据统计功能，如某台售货机上小说的下载量、下载人数等

### 2011.12 - 2013.05：宜邻美食

* 工作内容：
	- 宜邻美食iPhone用户端
		1. 功能主要是送货区域浏览、店铺列表、店铺菜品列表、用户订单相关功能
		2. 学习Objective-C，能够利用xib和storybord开发一些简单的iOS app
  	- 宜邻美食Android用户端
		1. 功能主要是送货区域浏览、店铺列表、店铺菜品列表、用户订单相关功能
		2. 该项目第一次尝试独立搭建项目框架、封装网络请求
  	- 宜邻美食Android商家端
		1. 功能主要是菜品添加删除修改列表、店铺管理、订单管理
		2. 项目难点在于订单推送，如何及时、不漏单将订单推送到商家客户端。项目采用MQTT来进行推送，通过MQTT的学习和使用，深刻理解一些关于HTTP请求的知识
		3. 该项目是第一次独立开发完成的Android项目，项目中初次使用Fragment，通过Fragment的使用，深刻理解Fragment相关知识。通过该项目锻炼，学会如何查找问题原因并解决问题
  	- 宜邻美食php后台及API
		1. 根据手机需要接口撰写API相关的代码
		2. 负责了后台一些简单的添加删除修改列表
	
### 2010.10 - 2011.12：成都凌点科技

* 工作内容：
	- YuePad项目
		1. YuePad是面向音乐家来设计的，设计的目的在于以后演出或者学习时，不需要带着乐谱，而只需要带有一个pad就可以进行音乐的演奏
		2. 主要负责开发存储/查找/删除MP3和乐谱文件以及MP3播放、歌词同步等功能
  	- Andrioid Launcher2.2源码进行定制开发
		1. 主要对桌面循环滑动、长按app进行位置互换
		2. 通过该项目，阅读很多Android源码，理解View的事件分发、，ContentProvider、自定义控件等知识

### 其他

* 2013.12 - 2014.03：[减肥帮Android客户端][3]
* 2014.01 - 2014.03：[减肥帮iOS客户端][4]
* 2013.10 - 2013.11：[掌上城市Android客户端][5] 

## 教育背景

 * 学历：本科
 * 专业：计算机科学与技术
 * 毕业院校：四川省攀枝花学院
 * 毕业时间：2011-07-01
  
  [1]:https://github.com/yangyugang/laputapp
  [2]:https://bitbucket.org/loopeer/laputa4php
  [3]:http://www.wandoujia.com/apps/cn.jfbang
  [4]:https://itunes.apple.com/us/app/jian-fei-bang/id837111939?ls=1&mt=8
  [5]:http://fir.im/a72n/info
  [6]:http://fir.im/dzll
  [7]:http://fir.im/didaaa
  [8]:http://fir.im/startuptool
  [9]:http://fir.im/apdt
  [10]:http://fir.im/cds4
  [11]:http://fir.im/xcalss
  [12]:http://fir.im/8w42
  [13]:http://www.wandoujia.com/apps/com.onairm.android
  [14]:http://fir.im/7kzx
  [15]:http://fir.im/rzzb
  [16]:http://fir.im/66zz
  [17]:http://fir.im/oe44
  [18]:http://fir.im/asbu
  [19]:https://github.com/ForoneTech/ForoneAdmin

