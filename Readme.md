## 杨玉刚

 * 手机：18612184602
 * 邮箱：<smartydroid@gmail.com>
 
## 教育背景

2007 - 2011 四川省攀枝花学院 计算机科学与技术-软件方向

## 工作经历
### 2014.03 - 至今：北京绿葡科技有限公司
---
* 负责公司Android产品研发和外包开发、期间设计了一套针对Android的应用架构，源码托管在[github][1]
* 前期参与公司服务端开发、基于Laravel框架搭建了一套后台框架，源码托管在[bitbucket][2]
* 负责组建iOS技术团队并实施开发工作

### 2013.05 - 2014.03：北京友宝昂莱科技有限公司
---
* [友乐下载Android客户端工厂版V1.0][17]
	- 功能主要是资源浏览和下载、手机话费充值，二维码兑奖。项目特色在于连接友宝售货机wifi可下载友宝售货机里面的资源内容，这样可在无网的情况下，为封闭式工厂的工人带来娱乐
	- 负责搭建项目基础框架：网络框架基于github源码，图片框架使用开源框架Android-Universal-Image-Loader
	- 负责项目百分之八十以上的功能开发
* [友乐下载Android客户端公交版V1.0][18]
	- 在工厂版本V1.0基础上增加断点下载的功能
* [友乐下载Android客户端工厂版V2.0][17]
	- 更换基础框架：网络请求、数据缓存以及图片缓存使用volley
* 友乐下载工厂版后台V1.0
	- 因初期人手不够，参与一部分后台功能开发。后台采取PHP开发

### 2011.12 - 2013.05：湖北聚众信息科技有限公司
---
* 宜邻美食iPhone用户端
	- 功能主要是送货区域浏览、店铺列表、店铺菜品列表、用户订单相关功能
	- 学习Objective-C，能够利用xib和storybord开发一些简单的iOS app
* 宜邻美食Android用户端
	- 功能主要是送货区域浏览、店铺列表、店铺菜品列表、用户订单相关功能
	- 该项目第一次尝试独立搭建项目框架、封装网络请求
* 宜邻美食Android商家端
	- 功能主要是菜品添加删除修改列表、店铺管理、订单管理
	- 项目难点在于订单推送，如何及时、不漏单将订单推送到商家客户端。项目采用MQTT来进行推送，通过MQTT的学习和使用，深刻理解一些关于HTTP请求的知识
	- 该项目是第一次独立开发完成的Android项目，项目中初次使用Fragment，通过Fragment的使用，深刻理解Fragment相关知识。通过该项目锻炼，学会如何查找问题原因并解决问题
* 宜邻美食php后台及API
	- 根据手机需要接口撰写API相关的代码
	- 负责了后台一些简单的添加删除修改列表
	
### 2011.07 - 2011.12：成都凌点科技
---
* YuePad项目
	- YuePad是面向音乐家来设计的，设计的目的在于以后演出或者学习时，不需要带着乐谱，而只需要带有一个pad就可以进行音乐的演奏
	- 主要负责开发存储/查找/删除MP3和乐谱文件以及MP3播放、歌词同步等功能
* Andrioid Launcher2.2源码进行定制开发
	- 主要对桌面循环滑动、长按app进行位置互换
	- 通过该项目，阅读很多Android源码，理解View的事件分发、，ContentProvider、自定义控件等知识
	
## 项目经验

###绿葡科技
---
* 2015.03 - 2015.04：[创业箱][8]
* 2015.01 - 2015.03：[聚店][10]
* 2014.12 - 2015.02：[六六货运][9]
* 2014.08 - 2014.12：[云借阅][16]
* 2014.11 - 2014.12：[嘀嗒][7]
* 2014.08 - 2014.10：[段子来了][6]
* 2014.05 - 2014.08：[多彩日记][15]
* 2014.04 - 2014.06：[明星互动][14]
* 2014.04 - 2014.05：[爱互动][13]
* 2014.03 - 2014.04：[测评工具][12]
* 2014.03 - 2014.06：[人社资源][11]

### 其他
---
* 2013.12 - 2014.03：[减肥帮Android客户端][3]
* 2014.01 - 2014.03：[减肥帮iOS客户端][4]
* 2013.10 - 2013.11：[掌上城市Android客户端][5] 
	
## 技能清单
 * 熟练设计模式，具有扎实的Java基础和良好的编程习惯 
 * 熟悉Android UI设计规范和一些Android基础框架，能够独立负责Android端设计与开发
 * 熟练使用Android Studio、Gradle、Git等开发工具
 * 熟悉iOS，可独立开发简单的IOS应用
 * 熟悉PHP，可使用PHP框架编写API、后台功能模块
 * 熟悉Linux基本命令使用，能够独立部署服务端环境
 * 熟悉Java Web开发，可基于Struts2、Spring、Hibernate开发Web程序
 * 熟悉Mysql、Oracle、SQLite3数据库的使用
 * 了解OSCache、Velocity、SSI、Compass、Lucene、EJB3、Jbpm、dom4j、log4j

## 致谢

感谢您花时间阅读我的简历，期待能有机会和您共事。
  
  [1]:https://github.com/yangyugang/laputapp
  [2]:https://bitbucket.org/yangyugang/laputa4php
  [3]:http://www.wandoujia.com/apps/cn.jfbang
  [4]:https://itunes.apple.com/us/app/jian-fei-bang/id837111939?ls=1&mt=8
  [5]:http://fir.im/a72n/info
  [6]:http://fir.im/dzll
  [7]:http://fir.im/didaaa
  [8]:http://fir.im/startuptool
  [9]:http://fir.im/apdt
  [10]:http://fir.im/cds4
  [11]:http://fir.im/xcalss
  [12]:http://fir.im/8w42
  [13]:http://www.wandoujia.com/apps/com.onairm.android
  [14]:http://fir.im/7kzx
  [15]:http://fir.im/rzzb
  [16]:http://fir.im/66zz
  [17]:http://fir.im/oe44
  [18]:http://fir.im/asbu
	

